﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class MyFirstClass
    {
        static void Main(string[] args)
        {
            int[,] matrix =
            {
                { 1, 2, 3, 4 },
                { 10, 20, 30, 40},
                { 100, 200, 300, 400}
            };
            PrintArray(matrix);
            Console.ReadKey();
        }

        static void PrintArray (int[,] array)
        {
            Console.WriteLine();
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
